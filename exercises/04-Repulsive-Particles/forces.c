#include <math.h>
#include <float.h>
#include "forces.h"

static const double a = 0.2; /* particle radius for this collision problem */
static const double krepul = 100.;

void compute_forces_critical(int N, const double * restrict pos, double * restrict forces)
{
  int i;

  #pragma omp for
  for (i = 0; i < N; i++) {
    int j;
    for (j = i+1; j < N; j++) {
      const double * restrict ri = &pos[3*i];
      const double * restrict rj = &pos[3*j];
      double dx = remainder(ri[0]-rj[0],1.);
      double dy = remainder(ri[1]-rj[1],1.);
      double dz = remainder(ri[2]-rj[2],1.);
      double s2 = dx*dx + dy*dy + dz*dz;
      if (s2 < 4.*a*a) {
        double s = sqrt(s2);
        double f = krepul*(2.-s);

        #pragma omp critical(x)
        {
          forces[3*i+0] += f*dx/s;
          forces[3*i+1] += f*dy/s;
          forces[3*i+2] += f*dz/s;
          forces[3*j+0] -= f*dx/s;
          forces[3*j+1] -= f*dy/s;
          forces[3*j+2] -= f*dz/s;
        }
      }
    }
  }
}

void compute_forces_atomic(int N, const double * restrict pos, double * restrict forces)
{
  int i;

  #pragma omp for
  for (i = 0; i < N; i++) {
    int j;
    for (j = i + 1; j < N; j++) {
      const double * restrict ri = &pos[3*i];
      const double * restrict rj = &pos[3*j];
      double dx = remainder(ri[0]-rj[0],1.);
      double dy = remainder(ri[1]-rj[1],1.);
      double dz = remainder(ri[2]-rj[2],1.);
      double s2 = dx*dx + dy*dy + dz*dz;
      if (s2 < 4.*a*a) {
        double s = sqrt(s2);
        double f = krepul*(2.-s);

        #pragma omp atomic
        forces[3*i+0] += f*dx/s;
        #pragma omp atomic
        forces[3*i+1] += f*dy/s;
        #pragma omp atomic
        forces[3*i+2] += f*dz/s;
        #pragma omp atomic
        forces[3*j+0] -= f*dx/s;
        #pragma omp atomic
        forces[3*j+1] -= f*dy/s;
        #pragma omp atomic
        forces[3*j+2] -= f*dz/s;
      }
    }
  }
}

void compute_forces_duplicate(int N, const double * restrict pos, double * restrict forces)
{
  int i;

  #pragma omp for
  for (i = 0; i < N; i++) {
    int j;
    for (j = 0; j < N; j++) {
      if (j == i) continue;
      const double * restrict ri = &pos[3*i];
      const double * restrict rj = &pos[3*j];
      double dx = remainder(ri[0]-rj[0],1.);
      double dy = remainder(ri[1]-rj[1],1.);
      double dz = remainder(ri[2]-rj[2],1.);
      double s2 = dx*dx + dy*dy + dz*dz;
      if (s2 < 4.* a*a) {
        double s = sqrt(s2);
        double f = krepul*(2.-s);

        forces[3*i+0] += f*dx/s;
        forces[3*i+1] += f*dy/s;
        forces[3*i+2] += f*dz/s;
      }
    }
  }
}
