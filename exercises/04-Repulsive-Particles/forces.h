#if !defined(FORCES_H)
#define FORCES_H
/* compute repulsive forces for N particles in 3D */
void compute_forces_critical(int N, const double * restrict pos, double * restrict forces);
void compute_forces_atomic(int N, const double * restrict pos, double * restrict forces);
void compute_forces_duplicate(int N, const double * restrict pos, double * restrict forces);

#endif
