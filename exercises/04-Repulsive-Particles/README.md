Exercise 04: Repulsive Particles
================================

Due: ~~Tuesday, September 5~~ Wednesday, September 6, 2017

---

1. Get a bitbucket fork of the [repo for just this
   exercise](https://bitbucket.org/cse6230fa17/ex04) by forking.  Include your
   gtusername in the name of the repo as shown in class: `cse6230fa17-ex04-gtusername`.

2. Get a local copy of the repo for just this exercise by cloning.

3. Modify your code from [Exercise 03][1] to be on a periodic unit cube using the
   `remainder` function.  Replace my [`ex04.c`](ex04.c) with your updated code.

[1]: https://bitbucket.org/cse6230fa17/cse6230/src/master/exercises/03-Brownian-Motion

4. Modify your code to implement repulsive forces by implementing
   the `compute_forces()` function in [`forces.h`](forces.h) starting from the
   code in Professor Chow's lecture.

5. Compare the performance **for five time steps** between using critical
   sections, atomic operations, and independent iterations (which do twice the
   number of distance computations).  Summarize the results of the performance
   comparison in `ex04.pdf` in this directory.

6. Commit your changes and push to your bitbucket repo.

7. Transfer ownership of your bitbucket repo to the `cse6230fa17` team.
