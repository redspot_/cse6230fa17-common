#!/usr/bin/env python

prefetch=False

def get_bandwidths(N,Bandwidths):
    import subprocess as sub
    import os

    CFLAGS="-O3"
    if prefetch:
        CFLAGS = CFLAGS + " -fprefetch-loop-arrays"
    command = ['make','-C',os.path.abspath('../../notes/STREAM'),'runstreams','CFLAGS="' + CFLAGS + '"','N=%d' % N]
    make_process = sub.Popen(' '.join(command),shell=True,stdout=sub.PIPE,stderr = sub.PIPE)
    out, err = make_process.communicate()
    outwords = out.split()
    for i in range(len(outwords)):
        if outwords[i] == 'Copy:':
            Bandwidths[0].append(float(outwords[i + 1]) / 1.e3)
        if outwords[i] == 'Scale:':
            Bandwidths[1].append(float(outwords[i + 1]) / 1.e3)
        if outwords[i] == 'Add:':
            Bandwidths[2].append(float(outwords[i + 1]) / 1.e3)
        if outwords[i] == 'Triad:':
            Bandwidths[3].append(float(outwords[i + 1]) / 1.e3)

import matplotlib.pyplot as plt
import matplotlib.lines  as lines
import numpy as np

L1SizeInB = 32  * 2**10 # KiB
L2SizeInB = 256 * 2**10 # KiB
L3SizeInB = 3   * 2**20 # MiB
BPerW = 8
NMax = 4 * L3SizeInB / BPerW
WMax = 3 * NMax

CPUFrequency = 2.667e9 # Hz

RegisterWidthInB = 32
NumLoadPorts     = 2
NumStorePorts    = 1
L1BPerCycle      = RegisterWidthInB * (NumLoadPorts + NumStorePorts)
L2BPerCycle      = 64
L3BPerCycle      = 32

MemTransPerSecPerChan = 1866 * 1.e6
MemChannels           = 2
MemBPerTransaction    = 8

L1BW  = L1BPerCycle * CPUFrequency
L2BW  = L2BPerCycle * CPUFrequency
L3BW  = L3BPerCycle * CPUFrequency
MemBW = MemTransPerSecPerChan * MemChannels * MemBPerTransaction

fig, ax = plt.subplots()
if prefetch:
    ax.set_title("STREAM Bandwidth with Prefetching")
else:
    ax.set_title("STREAM Bandwidth without Prefetching")
ax.set_xscale("log")
ax.set_xlim([0.5 * L1SizeInB / BPerW,WMax])
ax.set_xlabel("Words")
ax.set_yscale("log")
ax.set_ylabel("GB/s")
#L1Line = lines.Line2D([0,L1SizeInB],[L1BW,L1BW])
#ax.add_line(L1Line)
ax.plot([0.5 * L1SizeInB / BPerW,L1SizeInB / BPerW],[L1BW / 1.e9,L1BW / 1.e9],'k')
ax.plot([L1SizeInB / BPerW,L2SizeInB / BPerW],[L2BW / 1.e9,L2BW / 1.e9],'k')
ax.plot([L2SizeInB / BPerW,L3SizeInB / BPerW],[L3BW / 1.e9,L3BW / 1.e9],'k')
ax.plot([L3SizeInB / BPerW,WMax],[MemBW / 1.e9,MemBW / 1.e9],'k')

Bandwidths = ([],[],[],[])
Ns = [int(1000 * 2**x) for x in np.linspace(0,11,num=11*2 + 1)]
for N in Ns:
    get_bandwidths(N,Bandwidths)
ax.set_ylim([0.5 * Bandwidths[3][-1],1.1*L1BW / 1.e9])

ax.plot([2 * n for n in Ns],Bandwidths[0],'bx',label='Copy')
ax.plot([2 * n for n in Ns],Bandwidths[1],'r+',label='Scale')
ax.plot([3 * n for n in Ns],Bandwidths[2],'go',label='Add')
ax.plot([3 * n for n in Ns],Bandwidths[3],'m*',label='Triad')

plt.legend()

plt.show()
