# Exercise 3: Non-Interacting Brownian Motion

* Due: Friday, September 1, 2017

* Please submit your exercise to T-Square.  To make the grading process more uniform:

    * Please make your submission typed

    * Only submit at most one attachment

    * If your submission is an attachment, make it a `pdf` file

---

(This assignment is from Prof. Chow's course)

* 1) Perform a Brownian dynamics simulation for `N = 10000` particles in 3-D
space initially randomly distributed in a unit box `(0, 1) x (0, 1) x (0 , 1)`.
The propagation formula for particle `i` is

```
x[i][:] = x[i][:] + sqrt(2 * dt) * y[i][:]
```

where `x[i][:]` is the position vector of particle `i`, `dt = 1.e-4` is the
time step size, and `y[i][:]` is a Brownian displacement, which has random
components uniformly distributed between `-1` and `1`.  Write a program that
performs 5000 time steps and outputs the average distance moved by the
particles every 500 time steps. Note that the average distance is expected to
grow proportionally with the square root of time.  Use OpenMP to parallelize
your program.

* 2) Submit a short report with the following sections:

    * Listing of your program.
    * Graph of average distance moved from 0 to 5000 time steps.
    * Table of the execution time for a few different loop scheduling options,
      and using an appropriate number of threads.
