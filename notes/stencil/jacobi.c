/* A 2D Jacobi smoother example, inspired by Hager & Wellein.  Given an m x n
 * array of rough (random) data, we will perform sweeps to smooth it out */
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <cse6230rand.h>
#if defined(_OPENMP)
#include <omp.h>
#endif
#include <tictoc.h>

#if !defined(JACOBI_N)
#define JACOBI_N 10002
#endif

#if !defined(JACOBI_NSTEPS)
#define JACOBI_NSTEPS 10
#endif

#if !defined(JACOBI_BLOCK)
#define JACOBI_BLOCK 10000
#endif

int main(int argc, char **argv)
{
  int           m = JACOBI_N;
  int           n = JACOBI_N;
  int           nsteps = JACOBI_NSTEPS;
  int           jblock = JACOBI_BLOCK;
  int           seed = 0;
  int           max_threads = 1;
  int           err;
  double        *x, **X;
  double        *y, **Y;
  cse6230rand_t *rands;

  if (argc > 1) m      = atoi(argv[1]);
  if (argc > 2) n      = atoi(argv[2]);
  if (argc > 3) jblock = atoi(argv[3]);
  if (argc > 4) nsteps = atoi(argv[4]);
  if (argc > 5) seed   = atoi(argv[5]);

  /* We don't smooth the end points, only the inner points.  So the number of
   * j indices that will be smoothes is (n - 2).  Don't let the block size be
   * bigger than this */
  jblock = (n - 2) < jblock ? (n - 2) : jblock;
  /* make sure the block size divides the update length */
  assert(!((n - 2) % jblock));


  /* allocate space for an array and create a double-indexed array with
   * pointers */
  err = posix_memalign((void **) &x,64,m * n * sizeof(*x));
  assert (!err);
  err = posix_memalign((void **) &X,64,m * sizeof(*X));
  assert (!err);
  for (int i = 0; i < m; i++) {
    X[i] = &x[n * i];
  }

  err = posix_memalign((void **) &y,64,m * n * sizeof(*x));
  assert (!err);
  err = posix_memalign((void **) &Y,64,m * sizeof(*X));
  assert (!err);
  for (int i = 0; i < m; i++) {
    Y[i] = &y[n * i];
  }


  /* allocate some thread random number generators */
#if defined(_OPENMP)
  max_threads = omp_get_max_threads();
#endif
  err = posix_memalign((void **) &rands,64,max_threads * sizeof(*rands));
  assert(!err);
  #pragma omp parallel
  {
    int thread_id = 0;

#if defined(_OPENMP)
    thread_id = omp_get_thread_num();
#endif
    cse6230rand_seed(thread_id + seed,&rands[thread_id]);
  }

  { /* make the random data */
    TicTocTimer timer;
    double      time;

    timer = tic();
    #pragma omp parallel
    {
      cse6230rand_t rand_copy;
      cse6230rand_t *rand;
      int thread_id = 0;

#if defined(_OPENMP)
      thread_id = omp_get_thread_num();
#endif

      /* we either copy the random number generator to this thread-private
       * variable (COPY_RAND) defined, or we work on it in place in the array
       */
#if !defined(COPY_RAND)
      rand = &rands[thread_id];
#else
      rand_copy = rands[thread_id];
      rand = &rand_copy;
#endif

      #pragma omp for
      for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
          X[i][j] = cse6230rand(rand);
        }
      }

      /* if we do copy the variable copy it back at the end so that the
       * changes to the state are recorded */
#if defined(COPY_RAND)
      rands[thread_id] = rand_copy;
#endif
    }
    time = toc(&timer);
    printf("Random right hand side time: %g\n", time);
  }

  { /* ==== SMOOTHING ==== */
    TicTocTimer timer;
    double      time = 0.;
    double      res = 0.;

    /* I put the parallel section outside of the time loop just to avoid
     * overhead */
    #pragma omp parallel
    for (int t = 0; t < nsteps; t++) {
      #pragma omp single
      timer = tic();
      #pragma omp for
      for (int jb = 1; jb < n - 1; jb += jblock) {
        for (int i = 1; i < m - 1; i++) {
          /* Make Y[i][j] the average of the four neighbors of X[i][j] */
          for (int j = jb; j < jb + jblock; j++) {
            Y[i][j] = (1./4.) * (X[i-1][j] + X[i+1][j] + X[i][j-1] + X[i][j+1]);
          }
        }
      }
      #pragma omp single
      time += toc(&timer);
      #pragma omp for
      for (int i = 1; i < m - 1; i++) {
        for (int j = 1; j < n - 1; j++) {
          /* Copy Y back into X */
          X[i][j] = Y[i][j];
        }
      }
    }

    /* X should start to become "smooth" with each point roughly equal to the
     * average of its neighbors */
    for (int i = 1; i < m - 1; i++) {
      for (int j = 1; j < n - 1; j++) {
        double val;

        val = X[i][j] -  (1./4.) * (X[i-1][j] + X[i+1][j] + X[i][j-1] + X[i][j+1]);
        res += val * val;
      }
    }
    printf("Jacobi iteration (%d x %d (blocked by %d), %d steps):\n  %g secs (%g lattice updates per sec), res %g\n", m, n, jblock, nsteps, time, ((double) (m - 2)) * ((double) (n - 2)) * ((double) nsteps) / time, sqrt(res));
  }

  free(Y);
  free(y);
  free(X);
  free(x);
  free(rands);

  return 0;
}

