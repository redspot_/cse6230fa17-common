# MPI

## Specifications/documentation:

- [http://mpi-forum.org/docs/](http://mpi-forum.org/docs/)

## Implementations

- [MPICH](http://www.mpich.org/)
- [MVAPICH](http://mvapich.cse.ohio-state.edu/)
- [Open-MPI](https://www.open-mpi.org/)
- [Intel MPI](https://software.intel.com/en-us/intel-mpi-library/)
- [IBM Spectrum / BG/Q](https://www.ibm.com/us-en/marketplace/spectrum-mpi)
- Other vendors...

## Tutorials

- [From Lawrence Livermore National Laboratory](https://computing.llnl.gov/tutorials/mpi/)
- [mpitutorial.com (with examples on github)](http://mpitutorial.com/)

## MPI Structure

- MPI is the Message Passing Interface:
    - a library of C routines (`MPI_Init()`, `MPI_Finalize()`)
    - that allow a program to interact with the MPI runtime process manager
    - which is started with a launcher (`mpiexec`, `mpirun`, cluser-specific)
    - and *implementation specific* environment variables

---

- Compared to OpenMP:
    - No `#pragma`s
        - an MPI program is an MPI program: there is no "turning off" MPI
        - though you can/should be able to run with one process
        - there are "dummy" implementations of MPI that only allow one process (`mpiuni` that ships with PETSc)
        - no need for "compiler support" for MPI: should work with any valid compiler
    - Environment variables are implementation specific, not part of the standard
        - Typically a last-step optimization rather than a primary control method
        - Affinity, choosing particular algorithms, buffer sizes, etc.
    - The launcher (`mpiexec`/`mpirun`)
        - A program called without the launcher is like a `#pragma omp for`
          outside of `#pragma omp parallel`: valid, but serial

## `helloworld.c`

- MPI can be treated like any other library:
    - `#include <mpi.h>` header file, `-I/path/to/mpi/include` when compiling
    - `-L/path/to/mpi/lib -lmpi` when linking

- But MPI has a convenience wrapper for the compiler `mpicc`, `mpic++`

---

- In general, `MPI_Init()` should be the first thing you do in an MPI program,
  `MPI_Finalize()` should be the last

### Hybrid parallelism

- See [Jeff Hammond's slides][MPI234] for examples of the different threading models
  possible with `MPI_Init_thread()`:
    - `MPI_THREAD_SINGLE`: a single threaded application
    - `MPI_THREAD_FUNNELED`: only the master thread calls MPI routines.
        - Threads must collaborate to pack send buffers / unpack receive
          buffers, incurring some overhead.
    - `MPI_THREAD_SERIALIZED`: only one thread calls MPI at a time.
    - `MPI_THREAD_MULTIPLE`: threads can call MPI routines at will.

---

## Point-to-Point Communication

- The [LLNL tutorial](https://computing.llnl.gov/tutorials/mpi/#Routine_Arguments) has manual pages for the routines we talked about today:

---

- Blocking: routines that only return when read/write buffers are safe to read-from/write-to:
    - [`MPI_Send`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Send.txt):
      implementation chooses when/if this routine can cause **deadlocks** by waiting to return for a matching recv that never posts.
    - [`MPI_Ssend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Ssend.txt):
      waits until a matching recv has posted, thus can cause deadlocks.
    - [`MPI_Bsend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Bsend.txt):
      buffers message before sending, so behavior is always *local* (does not depend on destination behavior).
      See [`pointtopoint-8.c`](pointtopoint/pointtopoint-8.c) about setting up the buffers properly.
    - [`MPI_Rsend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Rsend.txt):
      Only valid if a matching recv is ready at the destination. **Note**: rarely used, and so it seems not implemented properly.
      [`pointtopoint-9.c`](pointtopoint/pointtopoint-9.c) should fail, but MPICH treats this like `MPI_Ssend`.

---

- Blocking: routines that only return when read/write buffers are safe to read-from/write-to:
    - [`MPI_Recv`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Recv.txt)
    - [`MPI_Probe`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Probe.txt):
      useful when you have a desired *envelope* (source, tag,
      communicator), but do not know the data.
    - [`MPI_Get_count`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Get_count.txt):
      Combine with `MPI_Probe` to allocate buffers once the message size is known (see
      [`pointtopoint-3.c`](pointtopoint/pointtopoint-3.c))
    - [`MPI_Mprobe`](http://www.mpich.org/static/docs/latest/www3/MPI_Mprobe.html):
      thread safe probe that gives a handle to a *message*, which can then be received with
    - [`MPI_Mrecv`](http://www.mpich.org/static/docs/latest/www3/MPI_Mrecv.html).

---

- Non-blocking: routines return immediately, and create an `MPI_Request`.  Buffers/results are
  only safe when the request is completed. 
    - [`MPI_Isend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Isend.txt)
    - [`MPI_Issend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Issend.txt)
    - [`MPI_Ibsend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Ibsend.txt)
    - [`MPI_Irsend`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Irsend.txt)
    - [`MPI_Irecv`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Irecv.txt)
    - [`MPI_Iprobe`](https://computing.llnl.gov/tutorials/mpi/man/MPI_probe.txt)
    - [`MPI_Improbe`](http://www.mpich.org/static/docs/latest/www3/MPI_Improbe.html)
    - [`MPI_Imrecv`](http://www.mpich.org/static/docs/latest/www3/MPI_Imrecv.html)

---

- Handling the requests generated by non-blocking functions:
    - [`MPI_Wait`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Wait.txt):
      wait until the communication initiated by the non-blocking function
      reaches the same state as the non-blocking variant.  In other words,
      calling `MPI_Ixyz(...,&req); MPI_Wait(&req,&status);` should have the
      same blocking behavior as `MPI_Xyz(...,&status);`.
    - [`MPI_Test`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Test.txt):
      sets a Boolean true/false flag when the communication initiated by the
      non-blocking function reaches the same state as the non-blocking variant.
      In other words, calling `MPI_Ixyz(...,&req); MPI_Test(&req,&flag,&status);`
      will set flag to `1` only if `MPI_Xyz(...,&status);` would have completed.

---

- Handling the requests generated by non-blocking functions:
    - [`MPI_Waitall`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Waitall.txt):
      Wait until all requests in an array have completed.
    - [`MPI_Testall`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Testall.txt):
      True only if all requests in an array have completed.
    - [`MPI_Waitany`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Waitany.txt):
      Wait until at least one request in an array has completed, indicates which one it was.
    - [`MPI_Testany`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Testany.txt):
      True only if at least one request in an array has completed, indicates which one it was.
    - [`MPI_Waitsome`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Waitsome.txt):
      Wait until at least some requests in an array have completed, indicates which ones they were.
    - [`MPI_Testsome`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Testsome.txt):
      True only if at least some requests in an array has completed, indicates which ones they were.

## Communication Protocols

- [William Gropp's Tutorial at Argonne](https://www.mcs.anl.gov/research/projects/mpi/tutorial/perf/mpiperf/index.htm) has figures for protocols like we saw in class:
    - [Eager](https://www.mcs.anl.gov/research/projects/mpi/tutorial/perf/mpiperf/sld019.htm)
    - [Rendezvous](https://www.mcs.anl.gov/research/projects/mpi/tutorial/perf/mpiperf/sld022.htm)


## Performance

- [Professor Chow's Slides on MPI](https://www.cc.gatech.edu/~echow/ipcc/hpc-course/14_mpi.pdf) (slide 13)
  show time to send a message based on its length:
    - Short message time dominated by network *latency* `L`
    - Long message time dominated by reciprocal of network *bandwidth* `G = 1/B`
    - **Question in class**: why does the switch from eager to rendezvous protocol make it *faster*?
        - Answer: eager requires copying from MPI buffer to user buffer; rendezvous requires no copies.
          For large enough messages, `memcpy` takes longer than the network handshake (~`2L`).

## Machine Model

- [LogGP (Alexandrov et al. 97)](https://people.eecs.berkeley.edu/~mme/cs267-2016/papers/loggp.pdf):
  - `L`: Network latency
  - `o`: overhead of initiating send / completing recv
  - `g`: the "gap" between inserting consecutive messages into the network
  - `G`: the "Gap per byte": the reciprocal of network bandwidth
  - `P`: the number of processes
  - Not present: an idea of network "congestion" from multiple concurrent sends/recvs

## Collective Communication

- Like point-to-point routines, the collective communication routines have
  blocking and non-blocking variants.

---

- Blocking:

    - [`MPI_Barrier`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Barrier.txt):
      block program progress until all processes have reached the same barrier.
        - When processes are running on a shared-memory socket, this can be
          implemented efficiently: it can execute as quickly as a memory
          location that receives multiple writes can be updated in the cache
          for each process.  So the best-case latency of a barrier would be the
          cache-coherence latency of the socket.
        - For an abstract machine model like `LogGP` that has point-to-point primitives,
          Barrier can be implemented in a way that takes `O(log P)`.
        - Some machines like [IBM Blue Gene/Q](https://www.alcf.anl.gov/files/IBM_BGQ_Architecture_0.pdf)
          Have special networks dedicated to collective operations like barrier

---

- [`MPI_Bcast`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Bcast.txt):
  Copy a value (or vector of values) from one process to every process of the communicator.
- [`MPI_Scatter`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Scatter.txt):
  Scatter many values from one process to every processes of the communicator.
- [`MPI_Reduce`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Reduce.txt):
  Combine values from every process into a single value on one process using a reduction operation.
    - `MPI_SUM`
    - `MPI_MIN`
    - `MPI_MAX`
    - `MPI_PROD`
    - `MPI_LAND`: logical and
    - `MPI_BAND`: bitwise and
    - `MPI_LOR`: logical or
    - `MPI_BOR`: bitwise or
    - `MPI_LXOR`: logical exclusive or
    - `MPI_BXOR`: bitwise exclusive or
    - `MPI_MINLOC`: operates on `(value, rank)` pairs, returns the minimum and the rank from which the minimum originated
    - `MPI_MAXLOC`: operates on `(value, rank)` pairs, returns the maximum and the rank from which the maximum originated
    - `MPI_REPLACE`: returns the second input `replace(a,b) := b`

---

- [`MPI_Gather`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Gather.txt):
  Gather many values from every processes of the communicator to one process.
- [`MPI_Allreduce`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Allreduce.txt):
  Combine values from every process into a single value on every process using a reduction operation.
- [`MPI_Allgather`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Allgather.txt):
  Gather values from every process to every process.
- [`MPI_Alltoall`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Alltoall.txt):
  Every process send a unique value to every other process.

---

- Nonblocking (a new feature in MPI-3):
    - [`MPI_Ibarrier`](http://www.mpich.org/static/docs/latest/www3/MPI_Ibarrier.html)
    - [`MPI_Ibcast`](http://www.mpich.org/static/docs/v3.1/www3/MPI_Ibcast.html)
    - [`MPI_Iscatter`](http://www.mpich.org/static/docs/latest/www3/MPI_Iscatter.html)
    - [`MPI_Ireduce`](http://www.mpich.org/static/docs/latest/www3/MPI_Ireduce.html)
    - [`MPI_Igather`](http://www.mpich.org/static/docs/latest/www3/MPI_Igather.html)
    - [`MPI_Iallreduce`](http://www.mpich.org/static/docs/latest/www3/MPI_Iallreduce.html)
    - [`MPI_Iallgather`](http://www.mpich.org/static/docs/latest/www3/MPI_Iallgather.html)
    - [`MPI_Ialltoall`](http://www.mpich.org/static/docs/latest/www3/MPI_Ialltoall.html)

---

- Variable sizes: scatter, gather and all-to-all routines have variants for a
  variable message size per process:
    - [`MPI_Scatterv`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Scatterv.txt)
    - [`MPI_Gatherv`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Gatherv.txt)
    - [`MPI_Allgatherv`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Allgatherv.txt)
    - [`MPI_Alltoallv`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Alltoallv.txt)
    - [`MPI_Iscatterv`](http://www.mpich.org/static/docs/latest/www3/MPI_Iscatterv.html)
    - [`MPI_Igatherv`](http://www.mpich.org/static/docs/latest/www3/MPI_Igatherv.html)
    - [`MPI_Iallgatherv`](http://www.mpich.org/static/docs/latest/www3/MPI_Iallgatherv.html)
    - [`MPI_Ialltoallv`](http://www.mpich.org/static/docs/latest/www3/MPI_Ialltoallv.html)

---

- Scans: [Prefix sum](https://en.wikipedia.org/wiki/Prefix_sum) operations over
  values distributed between proceses
    - [`MPI_Scan`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Scan.txt):
      the reduction over myself and all lesser ranked processes of values stored by each
    - [`MPI_Exscan`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Exscan.txt):
      the reduction over all lesser ranked processes of values stored by each


## Dense matrix vector product

- [notes/mpi/dmv](https://bitbucket.org/cse6230fa17/cse6230/src/master/notes/mpi/dmv) has an example
  of achieving the same end result -- a distributed dense matrix vector product
  -- using different data layouts (row vs. column partition) and communication
  patterns (blocking vs. non-blocking, collective vs. point to point)

    - Also demonstrates [`MPI_Wtime`](https://computing.llnl.gov/tutorials/mpi/man/MPI_Wtime.txt), 
      niche communication routines not mentioned above
    - Demonstrates *overlapping* communication and computation, a typical
      strategy to achieving good performance when communication dominates the
      runtime of a program

## Remote memory access

- We worked from these notes in class, starting on slide 20:
    - [https://htor.inf.ethz.ch/teaching/mpi_tutorials/isc16/hoefler-balaji-isc16-advanced-mpi.pdf](https://htor.inf.ethz.ch/teaching/mpi_tutorials/isc16/hoefler-balaji-isc16-advanced-mpi.pdf)

## Additional Reading

- [http://mpi-forum.org/docs/](http://mpi-forum.org/docs/)
- [From Lawrence Livermore National Laboratory](https://computing.llnl.gov/tutorials/mpi/)
- [mpitutorial.com (with examples on github)](http://mpitutorial.com/)
- [Hammond, Jeff, 2016. "MPI: As Easy as 2, 3, 4".][MPI234]
- [Performance Notes by William Gropp at Argonne](https://www.mcs.anl.gov/research/projects/mpi/tutorial/perf/mpiperf/index.htm)
- [Professor Chow's Slides on MPI](https://www.cc.gatech.edu/~echow/ipcc/hpc-course/14_mpi.pdf)
- [Alexandrov et al. 97, "LogGP: Incorporating Long Messages into the LogP Model".](https://people.eecs.berkeley.edu/~mme/cs267-2016/papers/loggp.pdf)
- [Balaji, Pavan and dHoefler, Torsten, 2016.](https://htor.inf.ethz.ch/teaching/mpi_tutorials/isc16/hoefler-balaji-isc16-advanced-mpi.pdf)

[MPI234]: http://scisoftdays.org/pdf/2016_slides/hammond.pdf
