
#include <stdio.h>
#include <unistd.h>
#include "simplelib.h"

int main (int argc, char **argv)
{
  pid_t fork_id = 0, my_id;

  fork_id = fork();

  my_id = getpid();

  if (!fork_id) {
    sleep(1);
    printf("I am the child, my PID is %d and my PPID is %d\n", my_id, getppid());
  } else {
    printf("I am the parent, my PID is %d and my child's pid is %d\n", my_id, fork_id);
  }

  /* comment this out to see when unmodified data is shared */
  for (size_t i = 0; i < 500000000; i++) {
    programdata[i] = my_id;
  }

  sleep(10);

  if (!fork_id) {
    printf("Read from data %d\n", programdata[0]);
  } else {
    sleep(1);
    printf("Read from data %d\n", programdata[0]);
  }

  return 0;
}
