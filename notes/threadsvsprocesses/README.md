# Threads vs. Processes

## Our experience with threads through OpenMP

### Interface

- In our experience with OpenMP, we saw that it can program according to a PRAM model.

    - The default behavior for shared data *concurrent read, concurrent write* (CRCW) (i.e., race conditions are possible)
    - Through `critical` and `atomic` directives, one can enforce *concurrent read, exclusive read* (CREW)

- What makes it possible?

    - The POSIX threading model enables a shared memory space.
    - Hardware support for maintaining *cache coherence*

---

- The promise of OpenMP is to take a serial program and make it parallel.

    - Recall the suggestion that a program should be valid if compiled without OpenMP support.
    - The hope is for only a light sprinkling of `#pragma`s and API calls to dust the program.

- The ephemeral nature of fork-join threads makes them easy to insert, but also
  difficult to apply to general parallel programming.

    - Many parallel algorithms have multiple parallel sections with some thread *state* that should persist between them.

---

~~~
#pragma omp parallel
{
  /* work, work, work */

  /* outcome of work thus far affects state */
  MyState state[omp_get_thread_num()] = write_state(); 
}

something_inherently_sequential();

#pragma omp parallel
{
  /* I really wish I had state[] right about now */
}
~~~

---

- Two basic solutions: keep track of state outside of parallel sections:

~~~
MyState state[omp_get_max_threads()];

#pragma omp parallel
{
  /* work, work, work */

  /* outcome of work thus far affects state */
  state[omp_get_thread_num()] = write_state();
}

something_inherently_sequential();

#pragma omp parallel
{
  /* Hooray, I can use state: as long as there are
     the same number of threads */
}
~~~

---

- This gets harder if we want to take advantage of dynamic/nested parallelism:

---

~~~
/* What if there is a parallel region around this
   function? */
MyState state[omp_get_thread_limit()];

#pragma omp parallel
{
  /* work, work, work */

  #pragma omp parallel
  {
    /* Which state is mine? I might need omp_get_level(),
     * omp_get_ancestor_thread_num(), and
     * omp_get_team_size() to figure it out */
  }
}

something_inherently_sequential();

~~~

---

~~~

(continued)

#pragma omp parallel
{
  #pragma omp parallel
  {
    /* I'd better exactly reconstruct the parallelism if
     * I want to use the state correctly */ 
  }
}
~~~

---

- Two basic solutions: keep threads alive through many parallel working sets

~~~
#pragma omp parallel
{
  MyState state;

  /* work, work, work */
  state = write_state();

  #pragma omp single
  {
    something_inherently_sequential();
  }

  /* Same parallel region, state is still available */
}
~~~

---

- But we now must be very conscientious when adding functions to our program:

~~~
/* How many threads are calling me?
   Are they meant to collaborate? 
   Or is each call meant to be uncoordinated? */
void foo(ThreadContext ctx) /* I can insert
                               information about the
                               parallel state into the
                               function arguments */
{
  /* Or I could try to reconstruct
     the information I need */
  int num_threads = omp_get_num_threads();
  /* But do I know that they are all
     meant to work together? */
}
~~~

---

- **What if someone else tries to use your code?**

    - Read [Brown et al.][Librarization]: good software is used past its original purpose, in ways unintended.

        - Because it *can* be used past its original purpose.

    - Does their threading model match yours?

    - Long-lived threads seems to be the more workable solution: [See Karl Rupp's example][RuppTC]

### Performance

- While threading enable PRAM-modeled programming, what did we find affects performance?

    - Pinning threads to separate resources (CPUs, sockets) reduces contention in bottlenecks (cache space, memory channels, etc.).
    - Cache coherence / false sharing: try to avoid multiple writes to the same cache line (CRCW is slower than CREW).
    - Non-uniform memory access: have each thread work on memory pages in its own NUMA region.

- The more "shared" the shared memory, the slower the performance

---

- So why not use *separate processes* with separate *distributed* memory?

    - Is is slower to switch contexts between processes and between threads?
    - Do processes take up more memory than threads?

- [Read "Exascale Computing without Threads"][ExWOT]

---

- Threads share a virtual address space
- Processes have separate visual address spaces
    - But not necessarily separate physical address spaces:
        - physical addresses with read-only data in shared blocks can be
          referenced by multiple virtual addresses

---

- Switching between threads and between processes is almost the same:
    - With *physically* addressed caches, a cache can contain entries from multiple processes
    - The *Translation Lookaside Buffer* must be divided between processes

---

- See `simplefork`, `simplefork2`, and `simpleclone` examples

---

- So how do processes work together in parallel? [The Message Passing Interface
  (MPI)](http://mpi-forum.org/)

---

Threads vs. processes is more about whether the *programming model* matches your application as performance

- Threads: everything is shared by default
    - But private variables are possible (e.g., declared in `#pragma omp parallel` regions)

- Processes: everything is private by default
    - But (we shall see) shared memory is possible

---

## Additional Reading

- [Brown et al., 2014. "Run-Time Extensibility and Librarization of Simulation Software"][Librarization]
- [Rupp, Karl, 2016. "Multi-Threading in C/C++: Implications on Software Library Design"][RuppTC]
- [Knepley et al., 2015. "Exascale Computing without Threads"][ExWOT]

[Librarization]: https://dx.doi.org/10.1109/MCSE.2014.95
[RuppTC]: https://www.karlrupp.net/2016/03/multithreading-software-library-design/
[ExWOT]: https://www.orau.gov/hpcor2015/whitepapers/Exascale_Computing_without_Threads-Barry_Smith.pdf
