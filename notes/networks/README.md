Networks
========

Additional Reading
------------------

* [Prof. Chow's notes](https://www.cc.gatech.edu/~echow/ipcc/hpc-course/HPC-networks.pdf)
* Kim, Dally, Scott, Abts, 2008. [Technology-Driven, Highly-Scalable Dragonfly Topology](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/34926.pdf)
