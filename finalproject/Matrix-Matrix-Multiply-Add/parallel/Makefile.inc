INCLUDES=$(PETSC_CC_INCLUDES)
# PGI Compiler
PGCXX=pgc++
PGCXXFLAGS=-fast -mp -acc -ta=tesla:cc60 -Minfo=accel $(INCLUDES)
PGCC=pgcc
PGCFLAGS=-fast -mp -acc -ta=tesla:cc60 -Minfo=accel $(INCLUDES)
CUDAC=nvcc
CUDAFLAGS=-g -Wno-deprecated-gpu-targets $(INCLUDES)
LDPETSC=-Wl,-rpath,${PETSC_LIB_DIR} -L${PETSC_LIB_DIR} -lpetsc
LDFLAGS=-Mcuda -lcurand $(LDPETSC) -lcublas
#-L$(CUDA_HOME)/lib64 -lcudart

.SUFFIXES:
.SUFFIXES: .c .o .cu

ifndef MAGIC_MAKE
.c.o:
	$(PGCC) $(PGCFLAGS) -c $<
.cu.o:
	$(CUDAC) $(CUDAFLAGS) -c $<
endif

.PHONY: lib2 clean

LIBBASE    = libmmma
MMLIBNAME    = ${LIBBASE}.a

${MMLIBNAME}: lib2

lib2: mmma_mp_acc.o
	ar -rs ${MMLIBNAME} $^

harness: harness.o ${MMLIBNAME}
	echo ${PETSC_LIB_DIR}
	$(PGCXX) -o $@ $(PGCFLAGS) $^ $(LDFLAGS)

clean:
	rm -f harness mmma_mp_acc.o harness.o libmmma.a

ifndef PETSC_CC_INCLUDES
include ${PETSC_DIR}/lib/petsc/conf/variables
endif
