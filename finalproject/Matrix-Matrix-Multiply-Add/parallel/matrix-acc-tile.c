// compilation on bridges:
//
// module load pgi
// pgcc -mp -acc -fast ta=tesla:cc35,cc60 -Minfo=accel -DCHECK  matrix-acc-tile.c -o matrix-acc-tile-ompcheck
#include <sys/time.h>
#include <stdio.h>
#include <omp.h>
#include <openacc.h>

#define SIZE 4000

double a[SIZE][SIZE];
double b[SIZE][SIZE];
double c[SIZE][SIZE];
double d[SIZE][SIZE];

int main()
{
  int i,j,k;
  struct timeval tim;
  double t1, t2;
  double tmp;
  
  // Initialize matrices.
  for (i = 0; i < SIZE; ++i) {
    for (j = 0; j < SIZE; ++j) {
      a[i][j] = (double)(i + j);
      b[i][j] = (double)(i - j);
      c[i][j] = 0.0f;
      d[i][j] = 0.0f;
    }
  }

  // Time stamp t1
  gettimeofday(&tim, NULL);
  t1=tim.tv_sec+(tim.tv_usec/1000000.0);  

  int threads = acc_get_num_devices(acc_device_not_host);
  omp_set_num_threads(threads);
#pragma omp parallel
  {
    int ii, jj, kk;
    int id, blocks, start, end;
    id = omp_get_thread_num();
    blocks = SIZE/threads;
    start = id * blocks;
    end = (id+1) * blocks;
    acc_set_device_num(id, acc_device_not_host);

    // Compute matrix multiplication.
#pragma acc data copyin(a[start:blocks][0:SIZE]) copyin(b) copy(c[start:blocks][0:SIZE])
#pragma acc kernels
#pragma acc loop independent tile(32,32)
    for (ii = start; ii < end; ++ii) {
      for (jj = 0; jj < SIZE; ++jj) {
        tmp=0.0;
#pragma acc loop reduction(+:tmp)
        for (kk = 0; kk < SIZE; ++kk) {
          tmp += a[ii][kk] * b[kk][jj];
        }
        c[ii][jj] = tmp;
      }
    }
  }

  // Time stamp t2, elapsed time OpenACC
  gettimeofday(&tim, NULL);
  t2=tim.tv_sec+(tim.tv_usec/1000000.0);
  printf("%.6lf seconds with OpenACC \n", t2-t1);

#ifdef CHECK
  // ****************
  // double-check the OpenACC result 
  // ****************
  
  // Time stamp t1
  gettimeofday(&tim, NULL);
  t1=tim.tv_sec+(tim.tv_usec/1000000.0);  

  // Perform the multiplication
#pragma omp parallel for default(none) shared(a,b,d) private(i,j,k)
  for (i = 0; i < SIZE; ++i) 
    for (j = 0; j < SIZE; ++j) 
      for (k = 0; k < SIZE; ++k) 
	d[i][j] += a[i][k] * b[k][j];

  // Time stamp t2, elapsed time OpenMP
  gettimeofday(&tim, NULL);
  t2=tim.tv_sec+(tim.tv_usec/1000000.0);
  printf("%.6lf seconds with OpenMP \n", t2-t1);
 
  // Check the OpenACC result matrix
  for (i = 0; i < SIZE; ++i)
    for (j = 0; j < SIZE; ++j)
      if(c[i][j] != d[i][j]) {
	printf("Error %d %d %f %f \n", i,j, c[i][j], d[i][j]);
	exit(1);
      }
  printf("OpenACC matrix multiplication test was successful!\n");
#endif
  
  return 0;
}
/* vi: set expandtab sw=2 ts=2 cindent: */
