#!/bin/sh
#SBATCH  -J tiletest-p100             # Job name
#SBATCH  -p GPU                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:p100:2                # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o tiletest-p100-%j.out      # Standard output and error log
pwd; hostname; date
module load pgi
module load cuda
#pgaccelinfo
pgcc -mp -acc -fast ta=tesla:cc35,cc60 -Minfo=accel -DCHECK  matrix-acc-tile.c -o matrix-acc-tile-ompcheck
export PGI_ACC_TIME=1
./matrix-acc-tile-ompcheck
date
