#!/bin/sh
#SBATCH  -J mmma-double-k80              # Job name
#SBATCH  -p GPU-shared                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:k80:1                 # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o mmma-double-k80-%j.out       # Standard output and error log

ulimit -c unlimited
module load pgi
module load cuda
module load petsc

if [ ! -f Makefile.cuda ]; then
  echo "MMMA_CUDA = 1" > Makefile.cuda
fi

make test_mmma

pwd; hostname; date
export PGI_ACC_NOTIFY=19

#nvprof --profile-from-start off --output-profile mmma-profile-${SLURM_JOB_ID}.nvvp --log-file mmma-profile-${SLURM_JOB_ID}.log --print-gpu-trace --analysis-metrics ./test_mmma
nvprof --profile-from-start off --print-gpu-trace ./test_mmma

date


#!/bin/sh
#SBATCH  -J harness-p100             # Job name
#SBATCH  -p GPU                   # Queue (RM, RM-shared, GPU, GPU-shared)
#SBATCH  -N 1                            # Number of nodes
#SBATCH --gres=gpu:p100:2                # GPU type and amount
#SBATCH  -t 00:05:00                     # Time limit hrs:min:sec
#SBATCH  -o harness-p100-%j.out      # Standard output and error log
#pgaccelinfo
#export PGI_ACC_TIME=1
#export PGI_ACC_DEBUG=1
