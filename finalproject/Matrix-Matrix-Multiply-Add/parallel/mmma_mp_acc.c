//pgcc -mp -acc -fast ta=nvidia -Minfo=accel mmma_cuda.cu -o mmma_cuda.o
#include <stdio.h>
#include <assert.h>
#include <petscsys.h>
#include "mmma.h"

#include <openacc.h>
#include <omp.h>
#include <cuda_profiler_api.h>
#include <device_launch_parameters.h>

//convert A[x][y] into A[x * x_len + y]
//#define IDX(ni, i, j) ((i)*(ni) + (j))
#define TILE_STRIDE 16

#pragma acc routine seq
inline
int IDX(int LDA, int row, int col) {
  return (row * LDA) + col;
}

//mmma: ignore
//M,N,R: actual sizes of matrices
//xBlock: numDevices number of pre-allocated array of MatrixBlock's
//XLocal_p: XLocal_p[device] is double*, acc_malloc into it
int MMMAGetMatrixArraysDoubleDeviceOneGPU(MMMA mmma, size_t M, size_t N, size_t R,
                                    int numDevices,
                                    struct MatrixBlock* cBlock,
                                    struct MatrixBlock* aBlock,
                                    struct MatrixBlock* bBlock,
                                    double **CLocal_p, double **ALocal_p, double **BLocal_p)
{
  cBlock[0].rowStart = 0;
  cBlock[0].rowEnd = M;
  cBlock[0].colStart = 0;
  cBlock[0].colEnd = N;
  aBlock[0].rowStart = 0;
  aBlock[0].rowEnd = M;
  aBlock[0].colStart = 0;
  aBlock[0].colEnd = R;
  bBlock[0].rowStart = 0;
  bBlock[0].rowEnd = R;
  bBlock[0].colStart = 0;
  bBlock[0].colEnd = N;
  acc_set_device_num(0, acc_device_not_host);
  ALocal_p[0] = acc_malloc( M*R * sizeof(double) );
  // printf("a=%p\n", (void*)ALocal_p[0]);
  BLocal_p[0] = acc_malloc( R*N * sizeof(double) );
  // printf("b=%p\n", (void*)BLocal_p[0]);
  CLocal_p[0] = acc_malloc( M*N * sizeof(double) );
  // printf("c=%p\n", (void*)CLocal_p[0]);
  for (int i = 1; i < numDevices; i++) {
    cBlock[i].rowStart = 0;
    cBlock[i].rowEnd = 0;
    cBlock[i].colStart = 0;
    cBlock[i].colEnd = 0;
    aBlock[i].rowStart = 0;
    aBlock[i].rowEnd = 0;
    aBlock[i].colStart = 0;
    aBlock[i].colEnd = 0;
    bBlock[i].rowStart = 0;
    bBlock[i].rowEnd = 0;
    bBlock[i].colStart = 0;
    bBlock[i].colEnd = 0;
    ALocal_p[i] = 0;
    BLocal_p[i] = 0;
    CLocal_p[i] = 0;
  }
  return 0;
}

int MMMAGetMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                    int numDevices,
                                    struct MatrixBlock* cBlock,
                                    struct MatrixBlock* aBlock,
                                    struct MatrixBlock* bBlock,
                                    double **CLocal_p, double **ALocal_p, double **BLocal_p)
{
  PetscFunctionBeginUser;
  assert(numDevices);
  // printf("M=%zu,N=%zu,R=%zu\n",M,N,R);
  size_t a_rows = M, a_cols = R;
  size_t b_rows = R, b_cols = N;
  size_t c_rows = M, c_cols = N;
  size_t local_rows = M / numDevices;
  size_t extra_rows = M % numDevices;
  size_t local_b_rows = R / numDevices;
  size_t extra_b_rows = R % numDevices;

  for (int d = 0; d < numDevices; d++) {
    size_t row_start = d * local_rows;
    size_t row_end = (d + 1) * local_rows;
    size_t b_row_start = d * local_b_rows;
    size_t b_row_end = (d + 1) * local_b_rows;
    //add any remainder onto the last device
    //example:
    //    M=17 rows
    //    3 gpus
    //    gpu0 = 5 rows, row_start = 0, row_end = 5
    //    gpu1 = 5 rows, row_start = 5, row_end = 10
    //    gpu2 = 5+2 rows, row_start = 10, row_end = 15+2
    if (d == numDevices - 1) {
      local_rows += extra_rows;
      row_end += extra_rows;
      b_row_end += extra_b_rows;
    }
    // printf("local_b_rows=%zu,extra_b_rows=%zu\n",local_b_rows,extra_b_rows);
    // printf("local_b_rows=%zu,extra_b_rows=%zu\n",local_b_rows,extra_b_rows);

    cBlock[d].rowStart = row_start;
    cBlock[d].rowEnd = row_end;
    cBlock[d].colStart = 0;
    cBlock[d].colEnd = c_cols;
    size_t local_c_size = local_rows * c_cols;
    // printf("C:start=%zu,end=%zu\n", row_start, row_end);
    // printf("C:rows=%i X cols=%i = %zu\n",local_rows,c_cols,local_c_size);
    assert(local_c_size);

    aBlock[d].rowStart = row_start;
    aBlock[d].rowEnd = row_end;
    aBlock[d].colStart = 0;
    aBlock[d].colEnd = a_cols;
    size_t local_a_size = local_rows * a_cols;
    // printf("A:start=%zu,end=%zu\n", row_start, row_end);
    // printf("A:rows=%i X cols=%i = %zu\n",local_rows,a_cols,local_a_size);
    assert(local_a_size);

    bBlock[d].rowStart = b_row_start;
    bBlock[d].rowEnd = b_row_end;
    bBlock[d].colStart = 0;
    bBlock[d].colEnd = b_cols;
    //allocate the full size of B on each device
    size_t local_b_size = b_rows * b_cols;
    // printf("B:start=%zu,end=%zu\n", b_row_start, b_row_end);
    // printf("B:rows=%i X cols=%i = %zu\n",b_rows,b_cols,local_b_size);
    assert(local_b_size);

    acc_set_device_num(d, acc_device_not_host);
    ALocal_p[d] = acc_malloc( local_a_size * sizeof(double) );
    BLocal_p[d] = acc_malloc( local_b_size * sizeof(double) );
    CLocal_p[d] = acc_malloc( local_c_size * sizeof(double) );
    // Shift pointer to the start of the sub-block
    BLocal_p[d] += b_row_start * b_cols;
  }

  PetscFunctionReturn(0);
}

int MMMARestoreMatrixArraysDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R,
                                        int numDevices,
                                        struct MatrixBlock cBlock[],
                                        struct MatrixBlock aBlock[],
                                        struct MatrixBlock bBlock[],
                                        double *CLocal_p[], double *ALocal_p[], double *BLocal_p[])
{
  PetscFunctionBeginUser;
  for (int d = 0; d < numDevices; d++) {
    acc_set_device_num(d, acc_device_not_host);
    if (ALocal_p[d] == 0) break;
    acc_free(ALocal_p[d]);
    // Shift pointer back to the base
    BLocal_p[d] -= bBlock[d].rowStart * bBlock[d].colEnd;
    acc_free(BLocal_p[d]);
    acc_free(CLocal_p[d]);
  }
  PetscFunctionReturn(0);
}


// multi-threaded, multi-GPU accelerated Matrix Matrix Multiply Add
int MMMAApplyDoubleDevice(MMMA mmma, size_t M, size_t N, size_t R, double alpha, int numDevices,
                          const struct MatrixBlock cBlock[],
                          const struct MatrixBlock aBlock[],
                          const struct MatrixBlock bBlock[],
                          double *CLocal[], const double *ALocal[], const double *BLocal[])
{
  PetscFunctionBeginUser;

  // Shift pointer back to the base
  const double* Bptr[numDevices];
  for (int d = 0; d < numDevices; d++)
    Bptr[d] = BLocal[d] - (bBlock[d].rowStart * bBlock[d].colEnd);

  printf("numDevices=%i\n", numDevices);
  // printf("copying array B to devices\n");
  int d, peer;
  for (int d = numDevices - 1; d >= 0; d--) {
    int Bsize = (bBlock[d].rowEnd - bBlock[d].rowStart)
      * bBlock[0].colEnd // colStart is always 0
      * sizeof(double);
    size_t shift = bBlock[d].rowStart * bBlock[d].colEnd;
    acc_set_device_num(d, acc_device_not_host);
    for (int offset = 1; offset < numDevices; offset++) {
      peer = (d + offset) % numDevices;
      // printf("d=%i -> peer=%i\n", d, peer);
      cudaMemcpyPeer(
          Bptr[peer] + shift, // shift down by rows
          peer, // dest
          BLocal[d], // data always at the top
          d, // src
          Bsize);
    }
  }
  // printf("done copying\n");

  printf("start threading\n");
  omp_set_num_threads(numDevices);
  #pragma omp parallel
  {
    int id = omp_get_thread_num();
    // printf("id=%i alpha=%f\n", id, alpha);
    acc_set_device_num(id, acc_device_not_host);
    double *c = CLocal[id];
    // printf("c=%p\n", (void*)c);
    const double *a = ALocal[id],
          *b = Bptr[id];
    // printf("a=%p\n", (void*)a);
    // printf("b=%p\n", (void*)b);
    int i, j, k;
    int i_start = cBlock[id].rowStart,
        i_end = cBlock[id].rowEnd,
        j_start = cBlock[id].colStart,
        j_end = cBlock[id].colEnd,
        k_start = 0,
        k_end = R;

    const double (*A)[k_end] = (const double (*)[k_end]) a;
    const double (*B)[j_end] = (const double (*)[j_end]) b;
    double (*C)[j_end] = (double (*)[j_end]) c;

    // printf("is=%i,ie=%i,js=%i,je=%i,ks=%i,ke=%i\n", i_start, i_end, j_start, j_end, k_start, k_end);
    // printf("M=%zu,N=%zu,R=%zu\n",M,N,R);
    // acc gang = cuda threadblock or streaming multiprocessor
    // acc worker = cuda warp, num_workers = gridDim.x
    // acc vector = cuda thread, vector_length = blockDim.x
    #pragma acc parallel vector_length(512) deviceptr(A,B,C)
    {
      #pragma acc loop independent collapse(2)
      for (i = 0; i < i_end - i_start; ++i) {
        for (j = j_start; j < j_end; ++j) {
          C[i][j] *= alpha;
        }
      }
    }

  cudaProfilerStart();
    #pragma acc parallel deviceptr(A,B,C)
    {
      #pragma acc loop independent tile(TILE_STRIDE,TILE_STRIDE)
      for (i = 0; i < i_end - i_start; ++i) {
        for (j = j_start; j < j_end; ++j) {
          double tmp = 0.0;
          //#pragma acc cache(A[i:i+TILE_STRIDE][k:k+TILE_STRIDE], B[k:k+TILE_STRIDE][j:j+TILE_STRIDE])
          #pragma acc loop reduction(+:tmp)
          for (k = k_start; k < k_end; k++) {
            tmp += A[i][k] * B[k][j];
          }
          C[i][j] += tmp;
        }
      }
    }
  cudaProfilerStop();
  }
  // printf("end threading\n");
  PetscFunctionReturn(0);
}

int MMMACreate(MPI_Comm comm, MMMA *mmma_p) { return 0; }
int MMMADestroy(MMMA *mmma) { return 0; }

/* vim: set expandtab sw=2 ts=2 cindent: */
