#include "Matrix.h"
#include <iostream>

extern "C"
int matrix_matrix_multiply_add_single (size_t m, size_t n, size_t r, float alpha, float *C, const float *A, const float *B)
{
    Matrix<float> a(m,r,A);
    Matrix<float> b(r,n,B);
    Matrix<float> c(m,n,C);

    c *= alpha;
    c += a*b;

    std::cout << "finished performing float" << std::endl;;

    memcpy (C, &c[0][0], m*n*sizeof(float));
    return 0;
}

