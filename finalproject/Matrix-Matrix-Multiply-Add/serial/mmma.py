m = 16
n = 16
r = 16
BS = 8
"""
A(m,r) = m * r
B(r,n) = r * n
C(m,n) = m * n

A[i][k] = A[i * r + k]
B[k][j] = B[k * n + j]
C[i][j] = C[i * n + j]
"""

print "setting up A and B"
A = []
Af = []
for i in range(m):
    row = []
    for j in range(r):
        A.append(float(i + j))
        row.append(float(i + j))
    Af.append(row)
B = []
for i in range(r):
    for j in range(n):
        B.append(float(i - j))
print "done"

print "setting up control_C"
control_C = []
for i in range(m):
    for j in range(n):
        control_C.append(0.)
print "done"
print "setting up C"
C = []
for i in range(m):
    for j in range(n):
        C.append(0.)
print "done"

print "doing linear version"
for i in range(0, m):
    for j in range(0, n):
        for k in range(0, r):
            control_C[i * n + j] += A[i * r + k] * B[k * n + j]
print "done"

print "doing chunked version"
slice = BS * BS
for i in range(0, m, BS):
    for j in range(0, n, BS):
        for k in range(0, r, BS):
            CSi = min(m, i + BS)
            CSj = min(n, j + BS)
            CSk = min(r, k + BS)
            print "chunk A[{}:{}][{}:{}]".format(i, BS, k, BS)
            print "slice A[{}:{}]".format(i * r + k, slice)
            print "chunk B[{}:{}][{}:{}]".format(k, BS, j, BS)
            print "slice B[{}:{}]".format(k * n + j, slice)
            print "chunk C[{}:{}][{}:{}]".format(i, BS, j, BS)
            print "slice C[{}:{}]".format(i * n + j, slice)
            chunk_t = []
            slice_t = []
            for aii in range(i, CSi):
                for akk in range(k, CSk):
                    chunk_t.append(A[aii * r + akk])
                    slice_t.append(Af[aii][akk])
                    if A[aii * r + akk] != Af[aii][akk]:
                        print "fail at {},{}".format(aii,akk)
            for ti, val in enumerate(chunk_t):
                if slice_t[ti] != val:
                    print "_t fail at {}".format(ti)
            print len(chunk_t), len(slice_t), slice
            for ii in range(i, CSi):
                for jj in range(j, CSj):
                    for kk in range(k, CSk):
                        #mesg = (
                        #    "C[{}][{}]"
                        #    " + (A[{}][{}]"
                        #    " * B[{}][{}])"
                        #).format(ii, jj, ii, kk, kk, jj)
                        #print mesg
                        #C[ii][jj] += A[ii][kk] * B[kk][jj]
                        C[ii * n + jj] += A[ii * r + kk] * B[kk * n + jj]
print "done"

print "comparing control_C and C"
for i in range(m):
    for j in range(n):
        if control_C[i * n + j] != C[i * n + j]:
            print "failed at C[{}][{}]".format(i, j)
print "done"
