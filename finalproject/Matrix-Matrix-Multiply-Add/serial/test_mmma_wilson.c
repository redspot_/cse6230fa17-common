/*
const char help[] = "Test driver for the correctness of matrix-matrix multiply-add implementation";

#include <petscviewer.h>
#include <petscblaslapack.h>
#include "mmma.h"

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 10;
  PetscInt       scale = 20;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Matrix-Matrix Multiply-Add Test Options", "test_mmma.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_mmma.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the matrices in the test", "test_mmma.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of matrix_matrix_multiply_add()\n", numTests);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar *Aa, *Ba, *Ca, *Da, *Ea;
    PetscReal    logm, logn, logr, alpha, margin;
    PetscReal    maxThresh;
    PetscInt     m, n, r, i;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscRandomSetInterval(rand, 0, (PetscReal) scale);CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logm);CHKERRQ(ierr);
    logn = scale - logm;

    ierr = PetscRandomSetInterval(rand, 0, PetscMin(logn, logm));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logr);CHKERRQ(ierr);

    m = (PetscInt) PetscPowReal(2., logm);
    n = (PetscInt) PetscPowReal(2., logn);
    r = (PetscInt) PetscPowReal(2., logr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: C = [%D x %D], A = [%D x %D], B = [%D x %D]\n", m, n, m, r, r, n);CHKERRQ(ierr);

    ierr = PetscMalloc5(m*n, &Ca, m*r, &Aa, r*n, &Ba, m*n, &Da, m*n, &Ea);CHKERRQ(ierr);

    ierr = PetscRandomSetInterval(rand, -1., 1.);CHKERRQ(ierr);

    ierr = PetscRandomGetValue(rand, &alpha);CHKERRQ(ierr);

    for (i = 0; i < m * n; i++) {
      ierr = PetscRandomGetValue(rand, &Ca[i]);CHKERRQ(ierr);
      Da[i] = Ca[i];
      Ea[i] = PetscAbsReal(Da[i]);
    }
    for (i = 0; i < m * r; i++) {
      ierr = PetscRandomGetValue(rand, &Aa[i]);CHKERRQ(ierr);
    }
    for (i = 0; i < r * n; i++) {
      ierr = PetscRandomGetValue(rand, &Ba[i]);CHKERRQ(ierr);
    }

#if defined(PETSC_USE_REAL_DOUBLE)
    ierr = matrix_matrix_multiply_add_double((size_t) m, (size_t) n, (size_t) r, alpha, Ca, Aa, Ba);CHKERRQ(ierr);
#elif defined(PETSC_USE_REAL_SINGLE)
    ierr = matrix_matrix_multiply_add_single((size_t) m, (size_t) n, (size_t) r, alpha, Ca, Aa, Ba);CHKERRQ(ierr);
#else
    SETERRQ(comm,PETSC_ERR_LIB,"Precision not supported by test");
#endif


    {
      PetscBLASInt mb = m, nb = n, rb = r;
      PetscReal one = 1.;

      PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Ba, &nb, Aa, &rb, &alpha, Da, &nb));
    }

    for (i = 0; i < m * r; i++) {
      Aa[i] = PetscAbsReal(Aa[i]);
    }
    for (i = 0; i < r * n; i++) {
      Ba[i] = PetscAbsReal(Ba[i]);
    }

    alpha = PetscAbsReal(alpha);

    {
      PetscBLASInt mb = m, nb = n, rb = r;
      PetscReal one = 1.;

      PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Ba, &nb, Aa, &rb, &alpha, Ea, &nb));
    }

    margin = PetscPowReal(((PetscReal) 1. + (PetscReal) PETSC_MACHINE_EPSILON),2. * (2. * r + 1)) - 1.;
    maxThresh = 0.;
    for (i = 0; i < m *  n; i++) {
      PetscReal res = Da[i] - Ca[i];
      PetscReal thresh = margin * Ea[i];

      maxThresh = PetscMax(thresh,maxThresh);
      if (PetscAbsReal(res) > thresh) {
        SETERRQ5(comm, PETSC_ERR_LIB, "Test %D failed error test at (%D,%D) threshold %g with value %g\n", test, i / n, i % n, (double) thresh, (double) res);
      }
    }
    ierr = PetscFree5(Ca, Aa, Ba, Da, Ea);CHKERRQ(ierr);

    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Maximum threshold: %g\n", (double) maxThresh);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPrintf(viewer, "Passed.\n");CHKERRQ(ierr);

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);CHKERRQ(ierr);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
*/
/*
    Test dimensions: C = [21628 x 48], A = [21628 x 1], B = [1 x 48]
    Test dimensions: C = [129 x 8127], A = [129 x 4], B = [4 x 8127]
    Test dimensions: C = [245641 x 4], A = [245641 x 3], B = [3 x 4]
    Test dimensions: C = [178 x 5858], A = [178 x 3], B = [3 x 5858]
    Test dimensions: C = [301411 x 3], A = [301411 x 1], B = [1 x 3]
    Test dimensions: C = [19 x 54511], A = [19 x 2], B = [2 x 54511]
    Test dimensions: C = [303504 x 3], A = [303504 x 1], B = [1 x 3]
    Test dimensions: C = [27091 x 38], A = [27091 x 2], B = [2 x 38]
    Test dimensions: C = [252 x 4158], A = [252 x 173], B = [173 x 4158]
    Test dimensions: C = [16 x 63578], A = [16 x 3], B = [3 x 63578]
 */
/*
m = 55
n = 7
r = 13
BS = 10
"""
A(m,r) = m * r
B(r,n) = r * n
C(m,n) = m * n

A[i][k] = A[i * r + k]
B[k][j] = B[k * n + j]
C[i][j] = C[i * n + j]
"""

print "setting up A and B"
A = []
for i in range(m):
    for j in range(r):
        A.append(float(i + j))
B = []
for i in range(r):
    for j in range(n):
        B.append(float(i - j))
print "done"

print "setting up control_C"
control_C = []
for i in range(m):
    for j in range(n):
        control_C.append(0.)
print "done"
print "setting up C"
C = []
for i in range(m):
    for j in range(n):
        C.append(0.)
print "done"

print "doing linear version"
for i in range(0, m):
    for j in range(0, n):
        for k in range(0, r):
            control_C[i * n + j] += A[i * r + k] * B[k * n + j]
print "done"

print "doing chunked version"
for i in range(0, m, BS):
    for j in range(0, n, BS):
        for k in range(0, r, BS):
            CSi = min(m, i + BS)
            CSj = min(n, j + BS)
            CSk = min(r, k + BS)
            print "chunk A[{}:{}][{}:{}]".format(i, CSi, k, CSk)
            print "chunk B[{}:{}][{}:{}]".format(k, CSk, j, CSj)
            print "chunk C[{}:{}][{}:{}]".format(i, CSi, j, CSj)
            for ii in range(i, CSi):
                for jj in range(j, CSj):
                    for kk in range(k, CSk):
                        #mesg = (
                        #    "C[{}][{}]"
                        #    " + (A[{}][{}]"
                        #    " * B[{}][{}])"
                        #).format(ii, jj, ii, kk, kk, jj)
                        #print mesg
                        #C[ii][jj] += A[ii][kk] * B[kk][jj]
                        C[ii * n + jj] += A[ii * r + kk] * B[kk * n + jj]
print "done"

print "comparing control_C and C"
for i in range(m):
    for j in range(n):
        if control_C[i * n + j] != C[i * n + j]:
            print "failed at C[{}][{}]".format(i, j)
print "done"

int err;
double *x;
int m, n;
err = posix_memalign((void **) &x,64,m * n * sizeof(*x));
assert (!err);
*/
#include <omp.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
// RNG
#include <openssl/rand.h>
#include <cse6230rand.h>

#include <petscviewer.h>
#include <petscblaslapack.h>

#include "mmma.h"

const int test_sizes[30] = {
  21628, 48, 1,
  129, 8127, 4,
  245641, 4, 3,
  178, 5858, 3,
  301411, 3, 1,
  19, 54511, 2,
  303504, 3, 1,
  27091, 38, 2,
  252, 4158, 173,
  16, 63578, 3,
};

// RNG
cse6230nrand_t _nrand;
cse6230nrand_t *nrand = &_nrand;
unsigned int _seed = 1889;

union ssl_double_rand {
  uint64_t i;
  unsigned char c[sizeof(uint64_t)];
};


int main(int argc, char** argv){
  union ssl_double_rand ssl_seed;
  ssl_seed.i = _seed;
  RAND_seed(ssl_seed.c, sizeof(ssl_seed.c));
  RAND_bytes(ssl_seed.c, sizeof(ssl_seed.c));
  _seed = (unsigned int) ssl_seed.i;
  cse6230nrand_seed(_seed,nrand);

  int m, n, r;
  //m = test_sizes[0];
  //n = test_sizes[1];
  //r = test_sizes[2];
  //m = test_sizes[24];
  //n = test_sizes[25];
  //r = test_sizes[26];
  m = 1000;
  n = 1000;
  r = 1000;
  double *a;
  double *b;
  double *c;
  double *d;
  double *e;
  double start;
  double end;

  printf("a(%i,%i), b(%i,%i), c(%i,%i)\n", m,r, r,n, m,n);
  printf("alloc\n");
  posix_memalign((void **) &c, 64, m * n * sizeof(*c));
  memset(c, 0, m * n * sizeof(*c));
  posix_memalign((void **) &d, 64, m * n * sizeof(*d));
  memset(d, 0, m * n * sizeof(*d));
  posix_memalign((void **) &e, 64, m * n * sizeof(*e));
  memset(e, 0, m * n * sizeof(*e));

  posix_memalign((void **) &a, 64, m * r * sizeof(*a));
  posix_memalign((void **) &b, 64, r * n * sizeof(*b));
  printf("done\n");

  //C[ii * n + jj] += A[ii * r + kk] * B[kk * n + jj];
  printf("filling\n");
  for(int i = 0; i < m; i++){
    for(int j = 0; j < r; j++){
      a[i*r+j] = (double)cse6230nrand(nrand);
    }
  }
  for(int i = 0; i < r; i++){
    for(int j = 0; j < n; j++){
      b[i*n+j] = (double)cse6230nrand(nrand);
    }
  }
  printf("done\n");

  {
    printf("calling BLASgemm\n");
    start = omp_get_wtime();
    PetscBLASInt mb = m, nb = n, rb = r;
    PetscReal one = 1.;
    double alpha = 1.0;

    PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N",
          &nb, &mb, &rb, &one,
          b, &nb,
          a, &rb,
          &alpha,
          e, &nb));
    end = omp_get_wtime();
    printf("Work took %f seconds\n", end - start);
  }

  printf("calling\n");
  start = omp_get_wtime();
  matrix_matrix_multiply_add_double(m, n, r, 1.0, c, a, b);
  end = omp_get_wtime();
  printf("Work took %f seconds\n", end - start);
  printf("calling\n");
  start = omp_get_wtime();
  omp_matrix_matrix_multiply_add_double(m, n, r, 1.0, d, a, b);
  end = omp_get_wtime();
  printf("Work took %f seconds\n", end - start);

  int ctr;
  double threshold = pow(10, -12);
  printf("comparing c and e\n");
  ctr = 0;
  for (int i = 0; i < m * n; i++) {
    double cv = c[i];
    double dv = e[i];
    double diff = fabs(dv - cv);
    if (diff > threshold) {
      printf("c!=d, i=%i,(%i,%i)\t", i, (int)(i / n), (int)(i % n));
      printf("c=%g,d=%g\t", cv, dv);
      printf("d-c=%g\n", diff);
      if (++ctr > 10) i = m * n;
    }
  }
  printf("comparing d and e\n");
  ctr = 0;
  for (int i = 0; i < m * n; i++) {
    double cv = d[i];
    double dv = e[i];
    double diff = fabs(dv - cv);
    if (diff > threshold) {
      printf("c!=d, i=%i,(%i,%i)\t", i, (int)(i / n), (int)(i % n));
      printf("c=%g,d=%g\t", cv, dv);
      printf("d-c=%g\n", diff);
      if (++ctr > 10) i = m * n;
    }
  }

  free(a);
  free(b);
  free(c);
  free(d);
}
