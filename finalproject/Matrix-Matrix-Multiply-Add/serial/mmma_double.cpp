#include "Matrix.h"

extern "C"
int matrix_matrix_multiply_add_double (size_t m, size_t n, size_t r, double alpha, double *C, const double *A, const double *B)
{
    Matrix<double> a(m,r,A);
    Matrix<double> b(r,n,B);
    Matrix<double> c(m,n,C);

    c *= alpha;
    c += a*b;

    memcpy (C, &c[0][0], m*n*sizeof(double));
    return 0;
}

