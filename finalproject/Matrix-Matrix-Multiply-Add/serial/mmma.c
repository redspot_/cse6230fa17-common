#include <omp.h>
#include <math.h>
#include <stdio.h>
// #define TYPE
#define FN(N,T) N##T
#define MMMA(T) FN(matrix_matrix_multiply_add_,T)
#define OMMMA(T) FN(omp_matrix_matrix_multiply_add_,T)

int MMMA(NAME)(size_t m, size_t n, size_t r, TYPE alpha, TYPE *C, const TYPE *A, const TYPE *B)
__attribute__((optimize("O3"))) //turn on -O3 even if not set by makefile
{
  for (int row = 0; row < m; row++)
  {
    for (int col = 0; col < n; col++)
    {
      C[row * n + col] *= alpha;
      for (int c = 0; c < r; c++)
      {
        C[row * n + col] += A[row * r + c] * B[c * n + col];
      }
    }
  }

  return 0;
}

void omp_matmul_depend(size_t BS, size_t m, size_t n, size_t r, TYPE alpha, TYPE C[m*n], const TYPE A[m*r], const TYPE B[r*n])
__attribute__((optimize("O3"))) //turn on -O3 even if not set by makefile
__attribute__((optimize("openmp"))) //turn on -fopenmp even if not set by makefile
{
  int i, j, k, ii, jj, kk;
  int ctr = 0;
  int CSi, CSj, CSk;
  int A_start, B_start, C_start;
  //TYPE (*Am)[m][r] = (TYPE (*)[m][r])A;
  //TYPE (*Bm)[r][n] = (TYPE (*)[r][n])B;
  //TYPE (*Cm)[m][n] = (TYPE (*)[m][n])C;
  const int slice = BS * BS;
  for (i = 0; i < m; i+=BS) {
    for (j = 0; j < n; j+=BS) {
      for (k = 0; k < r; k+=BS) {
        CSi = sz_min(m, BS + i);
        CSj = sz_min(n, BS + j);
        CSk = sz_min(r, BS + k);
        A_start = (i * r) + k;
        B_start = (k * n) + j;
        C_start = (i * n) + j;
        ctr++;
#pragma omp task private(ii, jj, kk) \
        depend ( in: A[A_start:slice], B[B_start:slice] ) \
        depend ( inout: C[C_start:slice] )
        //depend ( in: Am[i:BS][k:BS], Bm[k:BS][j:BS] )
        //depend ( inout: Cm[i:BS][j:BS] )
        {
          int t_num = omp_get_thread_num();
          printf("starting thread %i, ctr=%i\n", t_num, ctr);
          for (ii = i; ii < CSi; ii++ )
            for (jj = j; jj < CSj; jj++ ) {
              C[ii * n + jj] *= alpha;
              for (kk = k; kk < CSk; kk++ )
                C[ii * n + jj] += A[ii * r + kk] * B[kk * n + jj];
            }
          printf("  ending thread %i, ctr=%i\n", t_num, ctr);
        }
      }
    }
  }
}

int OMMMA(NAME)(size_t m, size_t n, size_t r, TYPE alpha, TYPE *C, const TYPE *A, const TYPE *B)
__attribute__((optimize("openmp"))) //turn on -fopenmp even if not set by makefile
{
  // calculate block size
  int L1_size = 64 * 1024;
  // 3 arrays, then 1/2 as best practice
  int L1_fit = L1_size / 3 / 2;
  int block_size = (int)sqrt(L1_fit / sizeof(TYPE));
  //block_size = 36; // for 8 byte
  //block_size = 52; // for 4 byte
  printf(
      "block_size = %i\n", block_size);
#pragma omp parallel
  {
#pragma omp single
    {
      //int nt = omp_get_num_threads();
      //    "thread count = %i\n", nt);
      omp_matmul_depend(block_size, m, n, r, alpha, C, A, B);
    }
  }
  return 0;
}
