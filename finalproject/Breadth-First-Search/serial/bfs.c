#include <stdlib.h>
#include "bfs.h"

struct _cse6230graph
{
  int dummy;
};


int graph_create (size_t num_edges, const int64_t (*edges)[2], cse6230graph *graph)
{
  cse6230graph g = NULL;

  g = (cse6230graph) malloc(sizeof(*g));
  if (!g) return 1;

  *graph = g;
  return 0;
}

int graph_destroy (cse6230graph *graph)
{
  free(*graph);
  *graph = NULL;
  return 0;
}

int breadth_first_search (cse6230graph graph, int num_keys, const int64_t *keys, int64_t **parents)
{
  return 0;
}
