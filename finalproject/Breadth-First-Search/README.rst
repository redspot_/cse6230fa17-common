====================
Breadth First Search
====================

:author: CSE 6230
:date:   Fall 2017

Definition
----------

Given an undirected graph :math:`G=(V,E)` specified as a list of edges given as
vertex pairs,

.. math::

  E=\{e_0 = (v_0,w_0),\dots,e_{|E|-1} = (v_{|E|-1},w_{|E|-1})\},

and given a starting vertex :math:`r`, a *breadth first search* :math:`B` is a
list of vertices in topological order by edge distance from :math:`r`.

Construction
------------

There are two timed components of performance: the first is constructing
:math:`G`, which may take any form, and a series of breadth first searches from
randomly determined roots that are unknown at the time :math:`G` is
constructed.
    
