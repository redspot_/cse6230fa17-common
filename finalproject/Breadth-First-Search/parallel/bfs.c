#include <petscsys.h>
#include "bfs.h"

struct _bfsgraph
{
  MPI_Comm comm;
};

int BFSGraphCreate(MPI_Comm comm, BFSGraph *graph_p)
{
  BFSGraph       graph = NULL;
  PetscErrorCode ierr;

  PetscFunctionBeginUser;
  ierr = PetscCalloc1(1,&graph); CHKERRQ(ierr);

  graph->comm = comm;

  *graph_p = graph;
  PetscFunctionReturn(0);
}

int BFSGraphDestroy(BFSGraph *graph_p)
{
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = PetscFree(*graph_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int BFSGraphGetEdgeArray(BFSGraph graph, size_t E, size_t *numEdgesLocal, int64_t (**elocal_p)[2])
{
  MPI_Comm       comm;
  int            size, rank;
  size_t         edgeStart, edgeEnd;
  size_t         numLocal;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  comm = graph->comm;

  ierr = MPI_Comm_size(comm, &size); CHKERRQ(ierr);
  ierr = MPI_Comm_rank(comm, &rank); CHKERRQ(ierr);

  edgeStart = (rank * E) / size;
  edgeEnd   = ((rank + 1) * E) / size;
  numLocal  = edgeEnd - edgeStart;

  ierr = PetscMalloc1(numLocal, elocal_p); CHKERRQ(ierr);
  *numEdgesLocal = numLocal;

  PetscFunctionReturn(0);
}

int BFSGraphRestoreEdgeArray(BFSGraph graph, size_t E, size_t *numEdgesLocal, int64_t (**elocal_p)[2])
{
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  ierr = PetscFree(*elocal_p); CHKERRQ(ierr);

  PetscFunctionReturn(0);
}

int BFSGraphSetEdges(BFSGraph graph, size_t E, size_t numEdgesLocal, const int64_t (*eLocal_p)[2])
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}

int BFSGraphGetParentArray(BFSGraph graph, size_t *numVerticesLocal, int64_t *firstLocalVertex, int64_t **parentsLocal)
{
  PetscFunctionBeginUser;

  *numVerticesLocal = 0;
  *firstLocalVertex = 0;

  *parentsLocal = NULL;

  PetscFunctionReturn(0);
}

int BFSGraphRestoreParentArray(BFSGraph graph, size_t *numVerticesLocal, int64_t *firstLocalVertex, int64_t **parentsLocal)
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}

int BFSGraphSearch(BFSGraph graph, int num_keys, const int64_t *key, size_t numVerticesLocal, int64_t firstLocalVertex, int64_t **parentsLocal)
{
  PetscFunctionBeginUser;
  PetscFunctionReturn(0);
}
